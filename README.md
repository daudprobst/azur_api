Code Repository for the API used in the AZUR project from the 2021 [Tech4Germany](tech.4germany.org) fellowship. The functionalities of this API will also be made accessible through an UI (seperate repository).

# AZUR

The German Bundestag uses mathematical methods to make sure that the debates and the work in the parliament reflect the strenghts of the individual parties. For example, parties that have received more votes, have more seats in parliamentary committees, have access to more office space in the official buildings, and have more speaking time in the parliament. AZUR is an API that aims to make these calculations more easily accessible and more transparent. 
